package com.br.peterramaldes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CamelExploringApplication {

	public static void main(String[] args) {
		SpringApplication.run(CamelExploringApplication.class, args);
	}

}
